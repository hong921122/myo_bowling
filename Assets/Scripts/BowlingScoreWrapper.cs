﻿using UnityEngine;
using System.Collections;

public class BowlingScoreWrapper {
	public BowlingScoreWrapper() {
		total_set_score = 0;
		score_data = new ArrayList();
	}

	public int current_set;
	public int total_set_score;
	public ArrayList score_data;
	public SpecialScore special_score;
	
	public enum SpecialScore {
		NONE,
		STRIKE,
		SPARE
	};
}
