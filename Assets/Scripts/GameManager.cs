﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameManager : MonoBehaviour {
	public GameObject bowling_plane;
	public GameObject bowling_ball;
	public GameObject bowling_pin;

	public BowlingScoreWrapper[] score_list;
	public int game_set;
	public int game_pin;
	public Text ui_text;

	public GameObject ball_prefab;
	public GameObject pin_prefab;
	public GUISkin scoreboard_skin;

	private BowlingScoreWrapper tmp_score;
	private BowlingScoreWrapper prev_score;
	private int total_game_score;
	private bool end_game_flag;

	// Use this for initialization
	void Start () {
		game_set = 0;
		game_pin = 0;
		ui_text.text = "0";
		tmp_score = new BowlingScoreWrapper();
		score_list = new BowlingScoreWrapper[10];
		end_game_flag = true;
	}
	
	// Update is called once per frame
	void Update () {
		bool flag = false;
		resetHub ();

		if(Input.GetKeyDown (KeyCode.R)) {
			resetGame();
		}
		Debug.Log (bowling_ball.activeSelf);
		if(bowling_ball.transform.position.y - bowling_plane.transform.position.y < 0 && end_game_flag) {
			for(int i =0; i<bowling_pin.transform.childCount; i++) {
				GameObject pin = bowling_pin.transform.GetChild(i).gameObject;
				if(pin.GetComponent<Rigidbody>().velocity != Vector3.zero) {
					if(pin.transform.position.y - bowling_plane.transform.position.y > 0) {
						flag = false;
						break;
					} else flag = true;
				}
				else flag = true;
			}
			if(flag)
				StartCoroutine("endGame");
		}
	}

	IEnumerator endGame() {
		end_game_flag = false;

		yield return new WaitForSeconds(2.0f);

		foreach(BowlingPin pin_script in bowling_pin.GetComponentsInChildren<BowlingPin>()) {
			if(pin_script.pin_drop_flag && !pin_script.pin_number_check) {
				game_pin += 1;
				ui_text.text = game_pin.ToString();
				pin_script.pin_number_check = true;
			}
		}

		tmp_score.current_set = game_set;
		if(game_pin == 10) {
			tmp_score.special_score = BowlingScoreWrapper.SpecialScore.STRIKE;
			score_list[game_set] = tmp_score;
			prev_score = score_list[game_set];
			calcTotalScore();
			resetGame ();
		} else if(tmp_score.score_data.Count == 0) {
			tmp_score.score_data.Add(game_pin);
			game_pin = 0;
			endSetGame ();
		} else {
			if((int)tmp_score.score_data[0] + game_pin == 10) {
				tmp_score.special_score = BowlingScoreWrapper.SpecialScore.SPARE;
			} else {
				tmp_score.special_score = BowlingScoreWrapper.SpecialScore.NONE;
				tmp_score.score_data.Add(game_pin);
			}
			score_list[game_set] = tmp_score;
			prev_score = score_list[game_set];
			calcTotalScore();
			resetGame ();
		}

		end_game_flag = true;
	}

	void resetHub() {
		ThalmicHub hub = ThalmicHub.instance;
		
		if(Input.GetKeyDown(KeyCode.Q)) {
			hub.ResetHub();
		}
	}

	void endSetGame() {
		Destroy (bowling_ball.gameObject);
		bowling_ball = Instantiate(ball_prefab);
	}

	void resetGame() {
		Destroy (bowling_ball.gameObject);
		Destroy (bowling_pin.gameObject);

		bowling_ball = Instantiate(ball_prefab);
		bowling_pin = Instantiate(pin_prefab);
		game_set++;
		game_pin = 0;
		tmp_score = new BowlingScoreWrapper();
	}

	void calcTotalScore() {
		for(int i =0; i<game_set+1; i++) {
			if(score_list[i].special_score == BowlingScoreWrapper.SpecialScore.NONE) {
				score_list[i].total_set_score = ((int)score_list[i].score_data[0] + (int)score_list[i].score_data[1]);
			} else if(score_list[i].special_score == BowlingScoreWrapper.SpecialScore.SPARE) {
				try {
					score_list[i].total_set_score = (int)score_list[i+1].score_data[0] + 10;
				} catch(UnityException ex) {
					continue;
				}
			} else if(score_list[i].special_score == BowlingScoreWrapper.SpecialScore.STRIKE) {
				try {
					score_list[i].total_set_score = (int)score_list[i+1].score_data[0]+ (int)score_list[i+1].score_data[1] + 10;
				} catch(UnityException ex) {
					continue;
				}
			}
		}
	}

	void OnGUI() {
		int total_score = 0;
		for(int i =0; i<score_list.Length; i++) {
			total_score += score_list[i].total_set_score;
			GUI.Label(new Rect(Screen.width * (0.03f + (0.07f*i)), Screen.height - 50, 100, 100), score_list[i].score_data[0].ToString(), scoreboard_skin.label);
			GUI.Label(new Rect(Screen.width * (0.05f + (0.07f*i)), Screen.height - 50, 100, 100), score_list[i].score_data[1].ToString(), scoreboard_skin.label);
			GUI.Label(new Rect(Screen.width * (0.04f + (0.07f*i)), Screen.height - 30, 100, 100), score_list[i].total_set_score.ToString(), scoreboard_skin.label);
		}
		GUI.Label(new Rect(Screen.width * 0.5f, Screen.height - 30, 100, 100), total_score.ToString(), scoreboard_skin.label);
	}
}
