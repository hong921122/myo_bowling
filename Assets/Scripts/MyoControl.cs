﻿using UnityEngine;
using System.Collections;

using LockingPolicy = Thalmic.Myo.LockingPolicy;
using Pose = Thalmic.Myo.Pose;
using UnlockType = Thalmic.Myo.UnlockType;
using VibrationType = Thalmic.Myo.VibrationType;
using MyoQuaternion = Thalmic.Myo.Quaternion;

public class MyoControl : MonoBehaviour {
    private struct QuaternionDelta
    {
        public Quaternion input;
        public System.DateTime timestamp;
    };

	public GameObject myo = null;
	public ThalmicMyo _myo;
	public float sensitivity_factor = 5.0f;
	public float velocity_factor = 200.0f;

	private Pose _lastPose;

	private float x_offset;
	private float y_offset;
	private float z_offset;

    private QuaternionDelta initial_rotation;

	// Use this for initialization
	void Start () {
		myo = GameObject.Find ("Myo");
		_myo = myo.GetComponent<ThalmicMyo>();
		_lastPose = Pose.Unknown;

		x_offset = MyoQuaternion.Yaw (_myo._myoQuaternion);
		y_offset = MyoQuaternion.Roll (_myo._myoQuaternion);
		z_offset = MyoQuaternion.Pitch (_myo._myoQuaternion);
	}

	//forward -> roll
	//left, right -> yaw
	//up, down -> pitch
	//accel
	// Update is called once per frame
	void Update () {
		_myo.unlocked = false;
		float pitch = MyoQuaternion.Pitch(_myo._myoQuaternion);
		float roll = MyoQuaternion.Roll(_myo._myoQuaternion);
		float yaw = MyoQuaternion.Yaw(_myo._myoQuaternion);

		if(_myo.pose == Thalmic.Myo.Pose.DoubleTap) {
			//Initialize
//			_myo.Vibrate(VibrationType.Short);
		} else if(_myo.pose == Thalmic.Myo.Pose.Fist) {
			_lastPose = _myo.pose;
			initial_rotation.input = new Quaternion(_myo._myoQuaternion.Y, _myo._myoQuaternion.Z, -_myo._myoQuaternion.X, -_myo._myoQuaternion.W);
            initial_rotation.timestamp = _myo._myoQuaternion.timestamp;

			this.transform.localPosition = new Vector3(this.transform.localPosition.x - (yaw - x_offset) * sensitivity_factor,
			                                           this.transform.localPosition.y,
			                                           this.transform.localPosition.z);

			x_offset = yaw;
			y_offset = roll;
			z_offset = pitch;
		} else if(_myo.pose == Thalmic.Myo.Pose.FingersSpread) {
            //this.GetComponent<Rigidbody>().AddForce( new Vector3(-1 * _myo.accelerometer.normalized.x * velocity_factor, 
            //                                                     1.0f,
            //                                                     _myo.accelerometer.normalized.z * velocity_factor));
                this.GetComponent<Rigidbody>().AddForce(new Vector3(-1 * (initial_rotation.input.x - _myo._myoQuaternion.Y) * velocity_factor,
                    1.0f,
                    -1*(initial_rotation.input.z + _myo._myoQuaternion.X) * velocity_factor
                    ));
            if (_lastPose == Pose.Fist)
            {
                this.GetComponent<Rigidbody>().AddForce(new Vector3(initial_rotation.input.x - _myo._myoQuaternion.Y,
                    1.0f,
                    initial_rotation.input.z + _myo._myoQuaternion.X
                    ));
                //this.GetComponent<Rigidbody>().AddForce(new Vector3(-1 * _myo.accelerometer.normalized.x * velocity_factor,
                //                                                     1.0f,
                //                                                     _myo.accelerometer.normalized.z * velocity_factor));
            }
			_lastPose = _myo.pose;
		} else {
		}
	}

	//delta
	Vector3 calcForce() {

		return new Vector3(0, 0, 0);
	}
}
