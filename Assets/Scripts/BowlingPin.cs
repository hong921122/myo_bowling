﻿using UnityEngine;
using System.Collections;

public class BowlingPin : MonoBehaviour {
	private bool collision_flag;
	public bool pin_drop_flag;

	[HideInInspector]
	public bool pin_number_check;

	// Use this for initialization
	void Start () {
		pin_number_check = false;
		collision_flag = false;
		pin_drop_flag = false;
	}
	
	// Update is called once per frame
	void Update () {

		if(collision_flag) {
			RaycastHit hit;
			Ray ray = new Ray(this.transform.localPosition, new Vector3(0, -1, 0));
			if(Physics.Raycast(ray, out hit)) {
			Debug.Log (hit.distance);
				if(hit.transform.tag == "plane") {

					//modify
					if(hit.distance > 0) {
						pin_drop_flag = true;
					} else {
						pin_drop_flag = false;
					}
				}
			} else {
				pin_drop_flag = false;
				if(this.transform.position.y - GameObject.Find("GameManager").GetComponent<GameManager>().bowling_plane.transform.position.y < 0)
					pin_drop_flag = true;
			}
		}
	}

	void OnCollisionEnter(Collision collision) {
		if(collision.transform.tag == "ball" || collision.transform.tag == "tagpin1") {
			collision_flag = true;
		}
	}
}
